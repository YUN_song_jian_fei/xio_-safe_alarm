switch_dict = {
    'salvagnini_1': True,
    'salvagnini_2': True,
    'line_rush': True,
    'line_1': True,
    'line_2': True,
    'prima_power_panel': True,
    'prima_power_laser': True
}

site_interrupt_dict = {
    'salvagnini_1': True,
    'salvagnini_2': True,
    'line_rush': True,
    'line_1': True,
    'line_2': True,
    'prima_power_panel': True,
    'prima_power_laser': True
}

pre_dict = {
    'salvagnini_1': False,
    'salvagnini_2': False,
    'line_rush': False,
    'line_1': False,
    'line_2': False,
    'prima_power_panel': False,
    'prima_power_laser': False
}

signal_dict = {
    'salvagnini_1': 0,
    'salvagnini_2': 0,
    'line_rush': 0,
    'line_1': 0,
    'line_2': 0,
    'prima_power_panel': 0,
    'prima_power_laser': 0
}

off_signal_dict = {
    'salvagnini_1': 0,
    'salvagnini_2': 0,
    'line_rush': 0,
    'line_1': 0,
    'line_2': 0,
    'prima_power_panel': 0,
    'prima_power_laser': 0
}

station_name_dict = {
    'salvagnini_1': '萨瓦尼尼-1',
    'salvagnini_2': '萨瓦尼尼-2',
    'line_rush': '通用线2-冲',
    'line_1': '通用线2-折1',
    'line_2': '通用线2-折2',
    'prima_power_panel': '普玛宝轿壁线',
    'prima_power_laser': '普玛宝激光线'
}

station_name_switch_dict = {
    'salvagnini_1': '萨瓦尼尼1√',
    'salvagnini_2': '萨瓦尼尼2√',
    'line_rush': '通用线2-冲√',
    'line_1': '通用线2-折1√',
    'line_2': '通用线2-折2√',
    'prima_power_panel': '普玛宝轿壁线√',
    'prima_power_laser': '普玛宝激光线√'
}

# video_stream_paths_dict = {
#     'salvagnini_1': 'rtsp://admin:hdu417417@192.168.2.5/Streaming/Channels/102',
#     'salvagnini_2': 'rtsp://admin:hdu417417@192.168.2.2/Streaming/Channels/102',
#     'line_rush': 'rtsp://admin:hdu417417@192.168.2.6/Streaming/Channels/102',
#     'line_1': 'rtsp://admin:hdu417417@192.168.2.7/Streaming/Channels/102',
#     'line_2': 'rtsp://admin:hdu417417@192.168.2.8/Streaming/Channels/102',
#     'prima_power_panel': 'rtsp://admin:hdu417417@192.168.2.3/Streaming/Channels/102',
#     'prima_power_laser': 'rtsp://admin:hdu417417@192.168.2.4/Streaming/Channels/102'
# }

video_stream_paths_dict = {
    # 'salvagnini_1': 'videos\\32.mp4',
    # 'salvagnini_2': 'videos\\33.mp4',
    # 'line_rush': 'videos\\34.mp4',
    # 'line_1': 'videos\\35.mp4',
    # 'line_2': 'G:\\image\\D08_20210923091350.mp4',
    'prima_power_panel': 'videos\\02.avi',
    'prima_power_laser': 'videos\\03.avi'
}

masks_paths_dict = {
    'salvagnini_1': 'images/masks/salvagnini_1.jpg',
    'salvagnini_2': 'images/masks/salvagnini_2.jpg',
    'line_rush': 'images/masks/line_rush.jpg',
    'line_1': 'images/masks/line_1.jpg',
    'line_2': 'images/masks/line_2.jpg',
    'prima_power_panel': 'images/masks/prima_power_panel.jpg',
    'prima_power_laser': 'images/masks/prima_power_laser.jpg'
}

max_object_bbox_area_dict = {
    'salvagnini_1': 15000,
    'salvagnini_2': 15000,
    'line_rush': 15000,
    'line_1': 15000,
    'line_2': 15000,
    'prima_power_panel': 15000,
    'prima_power_laser': 15000
}


# OPC 服务器 URL
opc_url = 'opc.tcp://10.19.3.49:49320'

# 是否连接OPC服务器，执行紧急停机
open_opc = False
# 开启邮箱OPC报警
open_email_warning = False
# 开启统计闯入次数和邮箱发送报告功能
open_email_report = False
# 开启数据库存储异常记录
open_mysql_save_record = False

nodes_dict = {
    'salvagnini_1': "",
    'salvagnini_2': "",
    'line_rush': "",
    'line_1': "",
    'line_2': "",
    'prima_power_panel': "",
    'prima_power_laser': ""
}

min_object_bbox_area_dict = {
    'salvagnini_1': 500,
    'salvagnini_2': 500,
    'line_rush': 500,
    'line_1': 500,
    'line_2': 500,
    'prima_power_panel': 500,
    'prima_power_laser': 500
}

excluded_objects_dict = {
    'salvagnini_1': [],
    'salvagnini_2': [],
    'line_rush': [],
    'line_1': [],
    'line_2': [],
    'prima_power_panel': [],
    'prima_power_laser': []
}

frame_shape = (480, 640)

vis_name = 'line_2'
prev_vis_name = vis_name

device_name = 'cuda:0'
img_size = 416  # size of each image dimension
config_path = 'configs/yolov3.cfg'  # path to model configs file
weights_path = 'weights/yolov3.weights'  # path to weights file
class_path = 'configs/coco.names'
conf_thres = 0.8  # object confidence threshold
nms_thres = 0.4  # iou threshold for non-maximum suppression

mysql_host = 'localhost'
mysql_user = 'root'
mysql_password = '123456'
mysql_db = 'xio'
mysql_interrupt_table = 'interrupt_cpp'

email_opc_warning_interval = 3600

wechat_send_interval = 30

inter_threshold = 0.15

open_wechat_bot = False

wechat_group = "机器人安全监测"

report_statistics_interval = 3600

server_name_dict = {
    'salvagnini_1': '192.168.0.25',
    'salvagnini_2': '192.168.0.2',
    'line_rush': '192.168.0.11',
    'line_1': '192.168.0.3',
    'line_2': '192.168.0.4',
    'prima_power_panel': '192.168.0.5',
    'prima_power_laser': '192.168.0.6'
}

server_port = 8080

# server_name = '192.168.2.201'
#
# server_port_dict = {
#     'salvagnini_1': 9090,
#     'salvagnini_2': 9091,
#     'line_rush': 9092,
#     'line_1': 9093,
#     'line_2': 9094,
#     'prima_power_panel': 9095,
#     'prima_power_laser': 9096
# }

alarm_link_success = 0
alarm_link_failed = 1

check_detection_process_interval = 65

update_detection_flag_interval = 3

alarm_instruction = True


